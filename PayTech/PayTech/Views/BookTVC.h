//
//  BookTVC.h
//  PayTech
//
//  Created by Allister Alambra on 04/01/2016.
//  Copyright © 2016 Allister Alambra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookTVC : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bookNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *publishLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@end
