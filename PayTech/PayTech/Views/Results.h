//
//  Results.h
//  PayTech
//
//  Created by Allister Alambra on 04/01/2016.
//  Copyright © 2016 Allister Alambra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Results : UIView
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *resultsTable;
@end
