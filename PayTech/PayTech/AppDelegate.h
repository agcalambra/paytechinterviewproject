//
//  AppDelegate.h
//  PayTech
//
//  Created by Allister Alambra on 04/01/2016.
//  Copyright © 2016 Allister Alambra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

