//
//  BookData.h
//  PayTech
//
//  Created by Allister Alambra on 04/01/2016.
//  Copyright © 2016 Allister Alambra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookData : NSObject

@property (strong, nonatomic) NSString *imageurl;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *publicationDate;
@property (strong, nonatomic) NSString *rating;

@end
