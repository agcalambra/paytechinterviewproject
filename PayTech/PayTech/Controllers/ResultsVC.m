//
//  ResultsVC.m
//  PayTech
//
//  Created by Allister Alambra on 04/01/2016.
//  Copyright © 2016 Allister Alambra. All rights reserved.
//

#import "ResultsVC.h"
#import "BaseViewController.h"
#import "BookTVC.h"
#import "BaseView.h"

@interface ResultsVC (){
    NSXMLParser *parser;
    NSMutableArray *feeds;
    NSMutableArray *subFeeds;
    NSMutableDictionary *item;
    NSMutableString *title;
    NSMutableString *link;
    NSMutableString *desc;
    NSMutableString *imageURL;
    NSMutableArray *totalFeed;
    NSMutableArray *filteredData;
    NSMutableString *pubDate;
    NSString *element;
}

@end

@implementation ResultsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Initialize view
    [self loadXibFromName:@"ResultsXib"];
    self.resultsView = (Results*)self.view;
    
    // Register Nib
//    [self.resultsView.resultsTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    // Assign delegates
    self.resultsView.searchBar.delegate = self;
    self.resultsView.resultsTable.dataSource = self;
    self.resultsView.resultsTable.delegate = self;
    
    feeds = [[NSMutableArray alloc] init];
    subFeeds = [[NSMutableArray alloc] init];
    filteredData = [[NSMutableArray alloc] init];
    
    _keyboardDismisser = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:_keyboardDismisser];
    
    
    [self readRSS];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard {
    [self.resultsView endEditing:YES];
}

#pragma mark - UISearchBarDelegate

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        filteredData = totalFeed;
    }
    else
    {
        filteredData = [[NSMutableArray alloc] init];
        
        for (NSDictionary* dict in totalFeed){
            NSRange titleRange = [[dict objectForKey:@"title"] rangeOfString:text options:NSCaseInsensitiveSearch];
            if(titleRange.location != NSNotFound)
            {
                [filteredData addObject:dict];
            }
        }
    }
    
    [self.resultsView.resultsTable reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return filteredData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"BookCell";
    
    BookTVC *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BookTVCXib" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.bookNameLabel.text = [[filteredData objectAtIndex:indexPath.row] objectForKey: @"title"];
    NSURL *url = [NSURL URLWithString:[[filteredData objectAtIndex:indexPath.row] objectForKey: @"imageURL"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img = [[UIImage alloc] initWithData:data];
    cell.imageView.image = img;
    cell.publishLabel.text = [[filteredData objectAtIndex:indexPath.row] objectForKey: @"pubDate"];
    cell.ratingLabel.text = [[filteredData objectAtIndex:indexPath.row] objectForKey: @"rating"];
    
    return cell;
    
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0;
}


#pragma mark - RSS Reading

- (void)readRSS {
   
    totalFeed = [[NSMutableArray alloc] init];
    
    // Hardcoded cuz darn it I'm running out of time
    // Should be in plist.
    // And dispatched asynchronously with at least GCD
    
    NSURL *url = [NSURL URLWithString:@"http://www.amazon.co.uk/gp/rss/bestsellers/books/72/ref=zg_bs_72_rsslink"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    url = [NSURL URLWithString:@"http://www.amazon.co.uk/gp/rss/bestsellers/books/62/ref=zg_bs_62_rsslink"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    url = [NSURL URLWithString:@"http://www.amazon.co.uk/gp/rss/bestsellers/books/63/ref=zg_bs_63_rsslink"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    url = [NSURL URLWithString:@"http://www.amazon.co.uk/gp/rss/bestsellers/books/426317031/ref=zg_bs_426317031_rsslink"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    url = [NSURL URLWithString:@"http://www.amazon.co.uk/gp/rss/bestsellers/books/270408/ref=zg_bs_270408_rsslink"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    url = [NSURL URLWithString:@"http://www.amazon.co.uk/gp/rss/bestsellers/books/270423/ref=zg_bs_270423_rsslink"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    if ([element isEqualToString:@"item"]) {
        
        item    = [[NSMutableDictionary alloc] init];
        title   = [[NSMutableString alloc] init];
        link    = [[NSMutableString alloc] init];
        pubDate    = [[NSMutableString alloc] init];
        imageURL = [[NSMutableString alloc] init];
        desc = [[NSMutableString alloc] init];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
    } else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
    } else if ([element isEqualToString:@"description"]) {
        [desc appendString:string];
    } else if ([element isEqualToString:@"pubDate"]) {
        [pubDate appendString:string];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    
    if ([elementName isEqualToString:@"item"]) {
        
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"(stars-4-5)"
                                      options:NSRegularExpressionCaseInsensitive
                                      error:&error];
        NSArray* matches = [regex matchesInString:desc options:0 range:NSMakeRange(0, [desc length])];
        
        regex = [NSRegularExpression regularExpressionWithPattern:@"(stars-4-0)"
                                      options:NSRegularExpressionCaseInsensitive
                                      error:&error];
        NSArray* subMatches = [regex matchesInString:desc options:0 range:NSMakeRange(0, [desc length])];
        
        regex = [NSRegularExpression regularExpressionWithPattern:@"<img src=\"[\\w\\W\\d\\D ]*.jpg\"" options:NSRegularExpressionCaseInsensitive error:&error];
        
        NSArray* imageMatches = [regex matchesInString:desc options:0 range:NSMakeRange(0, [desc length])];
        
        if(imageMatches.count>0){
            NSTextCheckingResult* match = [imageMatches objectAtIndex:0];
            NSString *tempURL = [desc substringWithRange:match.range];
            imageURL = [[NSMutableString alloc] initWithString:[tempURL substringWithRange:NSMakeRange(10, tempURL.length-11)]];
            [item setObject:imageURL forKey:@"imageURL"];
        }
        [item setObject:title forKey:@"title"];
        [item setObject:link forKey:@"link"];
        [item setObject:pubDate forKey:@"pubDate"];
        
        
        if([matches count]>0){
            [item setObject:@"Rating: 4.5" forKey:@"rating"];
            BOOL doesExist = NO;
            for(NSDictionary* dict in feeds){
                if([[dict objectForKey:@"title"] isEqualToString:title]){
                    doesExist = YES;
                }
            }
            if(!doesExist){
                [feeds addObject:[item copy]];
            }
        }
        else if([subMatches count]>0){
            [item setObject:@"Rating: 4.0" forKey:@"rating"];
            BOOL doesExist = NO;
            for(NSDictionary* dict in feeds){
                if([[dict objectForKey:@"title"] isEqualToString:title]){
                    doesExist = YES;
                }
            }
            if(!doesExist){
                [feeds addObject:[item copy]];
            }
        }
        
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    [totalFeed addObjectsFromArray:feeds];
    [totalFeed addObjectsFromArray:subFeeds];
    
    if(self.currentText.length>0){
        for(NSDictionary* dict in totalFeed){
            NSString* targettitle = [dict objectForKey: @"title"];
            if ([targettitle rangeOfString:self.currentText].location != NSNotFound) {
                [totalFeed removeObject:dict];
            }
        }
    }
    
    [self.resultsView.resultsTable reloadData];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
