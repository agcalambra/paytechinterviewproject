//
//  ResultsVC.h
//  PayTech
//
//  Created by Allister Alambra on 04/01/2016.
//  Copyright © 2016 Allister Alambra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Results.h"

@interface ResultsVC : BaseViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, NSXMLParserDelegate>

@property (nonatomic, strong) UITapGestureRecognizer *keyboardDismisser;
@property (nonatomic, strong) Results* resultsView;
@property (nonatomic, strong) NSString* currentText;

@end
